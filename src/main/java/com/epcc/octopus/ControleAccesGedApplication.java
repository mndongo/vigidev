package com.epcc.octopus;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.epcc.octopus.service.AccesGedService;

@SpringBootApplication
public class ControleAccesGedApplication implements CommandLineRunner {
	Logger logger = LoggerFactory.getLogger(ControleAccesGedApplication.class);

	@Autowired
	AccesGedService service;
	
	public static void main(String[] args) {
		SpringApplication.run(ControleAccesGedApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("*** start application ");
		int i = args.length;
		logger.info(" nbre d'argument  = "+i);
		String dateModifouExport= "";
		
		for(int j=0; j < args.length;j++) {
			dateModifouExport = args[0];
			logger.info("argument numero  ["+j+" ] : " +args[j]);	
		}
	
		// on prend tout le monde 
		//dateModifouExport="20190715";
		logger.info("date de modification  : "+dateModifouExport);
	
		service.findModifyCompteByDate(dateModifouExport);
		logger.info("fin de l'exécution du programme");
		
		

	}

}
