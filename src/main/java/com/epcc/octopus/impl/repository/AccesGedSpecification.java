package com.epcc.octopus.impl.repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.epcc.octopus.impl.repository.SearchCriteria;
import com.epcc.octopus.model.Compte;

public class AccesGedSpecification implements Specification<Compte>{

	/**
	 * @param criteria
	 */
	private SearchCriteria criteria;
	
	public AccesGedSpecification(SearchCriteria criteria) {
		super();
		this.criteria = criteria;
	}


	@Override
	public Predicate toPredicate(Root<Compte> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		if (criteria.getOperation().equalsIgnoreCase("=")) {
            return criteriaBuilder.equal(
              root.<String> get(criteria.getKey()), criteria.getValue().toString());
        }else if (criteria.getOperation().equalsIgnoreCase(">=")) {
            return criteriaBuilder.greaterThanOrEqualTo(
                    root.<String> get(criteria.getKey()), criteria.getValue().toString());
        }else if (criteria.getOperation().equalsIgnoreCase("<=")) {
        	return criteriaBuilder.greaterThanOrEqualTo(
        			root.<String> get(criteria.getKey()), criteria.getValue().toString());
                    }
		return null;
	}
	

}
