package com.epcc.octopus.repository;

import com.epcc.octopus.model.Compte; 
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface IAccesGedRepository extends JpaRepository<Compte, Long>, JpaSpecificationExecutor<Compte>{
	

}
