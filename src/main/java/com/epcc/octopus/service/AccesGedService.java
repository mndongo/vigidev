package com.epcc.octopus.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.epcc.octopus.util.TraitementUtil;
import com.epcc.octopus.impl.repository.AccesGedSpecification;
import com.epcc.octopus.impl.repository.SearchCriteria;
import com.epcc.octopus.model.Compte;
import com.epcc.octopus.repository.IAccesGedRepository;

@Service
public class AccesGedService {

	Logger logger = LoggerFactory.getLogger(AccesGedService.class);
	@Autowired
	IAccesGedRepository accesGedRepository;
	@Value("${folderLocation}")
	private String folderLocation;
	@Value("${modifyCompte}")
	private String modifyCompte;
	@Value("${expireCompte}")
	private String expireCompte;
	@Value("${templateExpire}")
	private String templateExpire;
	@Value("${templatemodify}")
	private String templatemodify;
	@Value("${filePropertie}")
	private String filePropertie;
	
	public void findModifyCompteByDate(String date) {
		logger.info(" ************** compt expire depuis 2 jours   ex :  ********* "+date);
		AccesGedSpecification spec = new AccesGedSpecification(new SearchCriteria("modifytimestamp", ">=", date));
		List<Compte> results = accesGedRepository.findAll(spec);
		//List<Compte> results = accesGedRepository.findAll();
		logger.info("nbre de compte dont la date a ete modifiee modifie : "+results.size());
		List<Compte> compteValid = results.stream()
				  .filter(valide->valide.getFdbadge()!=null)
				  .collect(Collectors.toList());
		logger.info("le nombre de compte valide possedant un fdbadge= "+compteValid.size());

		// determine le profile de l'utilisateur
		List<Compte> compteUsers = getProfile(compteValid,null);
		
		if(compteUsers.size()>0) {
			createTemplateCompteExpired(compteUsers,modifyCompte,templatemodify);
		}else {
			logger.info(" Il n'y pas d'utilisateur a exporter dont le compte est modifie au moins 2 jours ");
		}
	}
	
	private List<Compte> getProfile(List<Compte> results, Properties props) {
		
		List<Compte> compteWithProfil = new ArrayList<Compte>();
		Set<Compte> compteNonExporte = new HashSet<Compte>();
		Properties properties = new Properties();
		
			try {
				FileInputStream fis = new FileInputStream(new File(filePropertie));
				properties.load(fis);
								
			}catch (IOException e) {
				logger.info("fichier non trouvé ");
			}			

		String[] listEtablissement = properties.get("alma.etablissement").toString().split(",");

		for(String etab : listEtablissement) {
			//logger.info("etab is :"+etab);
		}
		for (Compte profileCompte : results) {
			
			StringBuilder buf = TraitementUtil.deleteFromSupannentiteaffectation(profileCompte);
			if (buf.length() == 0){
				compteNonExporte.add(profileCompte);
				logger.error("AccesGedService : Pas  de suppanentiteaffectation pour : "+profileCompte.getMail());
			}else {
				List<String> lAffectation = TraitementUtil.getStringToList(buf);
				if(!(profileCompte.getSupannentiteaffectationprincipale() == null) && ArrayUtils.contains(listEtablissement, profileCompte.getSupannentiteaffectationprincipale().trim())){
					//logger.info("$$$ uid mail = "+profileCompte.getUid() );
					//logger.info("%%% etab is : "+profileCompte.getSupannentiteaffectationprincipale().trim());
					for(String valueOfSupannEntiteAfectation : lAffectation) {
						if("EPCC".equals(profileCompte.getSupannentiteaffectationprincipale().trim())){	
							if("D0006".equals(valueOfSupannEntiteAfectation.trim())){
				//				logger.info("profile 5");
								profileCompte.setPersonalTitle("5");
								break;
							}else {
								// profile 2
					//			logger.info("profile 2");
								profileCompte.setPersonalTitle("2");
								//break;
							}

						}else {
							if(!"GED-LECTEURS".equals(profileCompte.getOu())){
								//profil 4
								profileCompte.setPersonalTitle("4");
							}else {
								// profil 3;
								logger.info("profile 3");
								profileCompte.setPersonalTitle("3");								
							}
						}
							
					}
						
				}else {
					logger.info("l'établissement : " +profileCompte.getSupannentiteaffectationprincipale() + " n'est pas un établissement membre valide");
					compteNonExporte.add(profileCompte);
				}
				
				compteWithProfil.add(profileCompte);
			}
		}
		return compteWithProfil;
	}

	private void createTemplateCompteExpired(List<Compte> results,String typeExport,String templateType) {
		
		/**
		 * Initialize engine and get template
		 */
		
		try{
			Properties p = new Properties();	
			p.setProperty("resource.loader", "class");
			p.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
			p.setProperty("input.encoding", CharEncoding.UTF_8);
			p.setProperty("output.encoding",CharEncoding.UTF_8);
			p.setProperty("response.encoding",CharEncoding.UTF_8);
			p.setProperty("default.contentType", "text/html; charset=UTF-8");
	
			Velocity.init( p ); 
			VelocityContext context = new VelocityContext();
			
			Template template = Velocity.getTemplate("templates/"+templateType);
			template.setEncoding(CharEncoding.UTF_8);
			// récupération de la template
			if(template != null) {
				logger.info("template is loaded");
				context.put("userList", results);
				context.put("total", results.size());
				
				// chargement de la template
				loadTemplateT(template, context,typeExport);
			}
		}catch( ResourceNotFoundException rnfe ){
			logger.error("la ressource demandée n'existe pas " +rnfe);
		}catch( ParseErrorException pee ) {
			logger.error("une erreur est survenue lors du parsing de la template " +pee);
		}catch( MethodInvocationException mie ){
			logger.info("erreur est apparue lors d'un appel à une directive "+mie);
	
		}
		logger.info("Fin createTemplateXml  dans la classe mère");
	}
	
	private void loadTemplateT(Template template, VelocityContext context,String type) {
		try {
			
			String FORMAT = "%1$tY%1$tm%1$td_%1$tHh%1$tMm%1$tSs";
			//20210315_13h27m21s_epcc_modifs.csv
			Calendar cal = Calendar.getInstance(); cal.setTime(Date.from(Instant.now()));
			
			String format = String.format( FORMAT, cal);
			StringBuilder builder = new StringBuilder();
			builder.append(folderLocation);
			builder.append(format);
			builder.append(type);
			FileWriter writer = new FileWriter(new File(builder.toString()));
			
			template.merge(context, writer);
			
			
			writer.flush();
			writer.close();
			File fileCreated = new File(folderLocation+format+type);
			if(fileCreated.exists()) {
				logger.info("file is created");
			}else {
				logger.error("could not created");
			}
			
			
		}catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error("le fichier n'est pas créé "+e.getMessage());
		}catch (FileAlreadyExistsException  e) {
			logger.error("file already exist");
		}catch (Exception e) {
			logger.error("");
		}
		
	}


}
